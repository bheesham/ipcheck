FROM rust:1-slim-buster as builder

WORKDIR /usr/src/ipcheck
COPY . .
RUN cargo install --profile release --path .

FROM debian:buster-slim
COPY --from=builder /usr/local/cargo/bin/ipcheck /usr/local/bin/ipcheck

EXPOSE 3000

CMD ["ipcheck"]
