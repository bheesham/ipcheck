# ipcheck

Returns your IP(v4/v6) address in plain text.

Demo: [ipcheck.fly.dev](https://ipcheck.fly.dev).
