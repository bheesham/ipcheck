//! ipcheck is an application which responds the IP address making the
//! connection.
//!
//! You could probably use this if you're using dynamic DNS.
use axum::{extract::ConnectInfo, http, response::IntoResponse, routing::get, Router};
use std::net::{Ipv4Addr, SocketAddr};
use std::process;
use tokio::signal;

const LISTEN_PORT: u16 = 3000;

/// If the Server ever bails out we'll return this error.
#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("runtime error")]
    RuntimeError(#[from] tokio::task::JoinError),
    #[error("unknown server error")]
    UnknownServerError(#[from] hyper::Error),
}

/// Read the client's IP address from the
/// [`X-Forwarded-For`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Forwarded-For)
/// header. If we can't read it from there then we return the IP address of
/// whoever connected to us.
///
/// This should handle either:
/// * running behind a load balancer/reverse proxy;
/// * exposed directly to the internet.
///
/// This, of course, assumes the load balancer/reverse proxy behaves nicely and
/// sets the header.
async fn index(
    ConnectInfo(addr): ConnectInfo<SocketAddr>,
    req_headers: http::HeaderMap,
) -> impl IntoResponse {
    let mut res_headers = http::HeaderMap::new();
    res_headers.insert(http::header::CONTENT_TYPE, "text/plain".parse().unwrap());
    res_headers.insert(http::header::CACHE_CONTROL, "no-cache".parse().unwrap());
    if let Some(forwarded_for_str) = req_headers.get("X-Forwarded-For") {
        if let Ok(forwarded_for) = forwarded_for_str.to_str() {
            if let Some(ip) = forwarded_for.split(',').next() {
                return (http::StatusCode::OK, res_headers, String::from(ip));
            }
        }
    }
    (http::StatusCode::OK, res_headers, addr.ip().to_string())
}

async fn new_app(addr: SocketAddr) -> Result<(), Error> {
    let app = Router::new().route("/", get(index));
    axum::Server::bind(&addr)
        .serve(app.into_make_service_with_connect_info::<SocketAddr>())
        .await?;
    Ok(())
}

fn handle_exit(res: Result<Result<(), Error>, tokio::task::JoinError>) {
    match res {
        Err(err) => {
            eprintln!("runtime exited unexpectedly: {}", err);
            process::exit(1);
        }
        Ok(inner) => match inner {
            Err(err) => {
                eprintln!("server exited unexpectedly: {}", err);
                process::exit(2);
            }
            _ => unreachable!("axum::Server returned an Ok(_) value"),
        },
    };
}

/// Where the magic happens.
#[tokio::main(flavor = "current_thread")]
async fn main() {
    let server_handle =
        tokio::spawn(async move { new_app(SocketAddr::from((Ipv4Addr::UNSPECIFIED, LISTEN_PORT))).await });
    tokio::select! {
        res = server_handle => {handle_exit(res);},
        _ = signal::ctrl_c() => {
            eprintln!("graceful exit requested");
            process::exit(0);
        }
    };
}
